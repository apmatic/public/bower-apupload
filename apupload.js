'use strict';

angular.module('ap.upload', ['ng', 'ui.router', 'ap.socket', 'ap.toaster'])

  .factory('$bytes', function() {
    return {
      toSize: function(bytes) {
        var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
        if (bytes === 0){return 'n/a';}
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if (i === 0){return bytes + ' ' + sizes[i];}
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
      }
    };
  })

  .factory('$upload', function($socket, $rootScope, $bytes, $toaster, $log, $http){
    var files = {count: 0, data: {}, fields:{}},
      fileList = {},
      streams = {},
      destroyed = [],
      toDestroy = [];

    var addListener = function(element, attrs, opts){
      if(window.File && window.FileReader){
        element.children()[0].addEventListener('change', function(evt){handleFileSelect(evt, attrs, opts);}, false);
        element[0].addEventListener('dragover', handleDragOver, false);
        // element[0].addEventListener('dragenter', handleDragEnter, false);
        // element[0].addEventListener('dragleave', handleDragLeave, false);
        element[0].addEventListener('drop', function(evt){handleDragFileSelect(evt, attrs, opts);}, false);
      }else{
        $toaster.pop('error','toaster.browser', 'toaster.notworking');
      }
    };

    function handleDragOver(evt) {
      evt.stopPropagation();
      evt.preventDefault();
      evt.dataTransfer.dropEffect = 'copy'; // explicitly show this is a copy
    }

    function handleFileSelect(evt, attrs, opts) {
      fileList = evt.target.files; // FileList object
      var fieldName = attrs.name;
      if(!files.fields[fieldName]){files.fields[fieldName] = {total: 0};} // set file length default

      checkFiles(opts, fileList, fieldName);
    }

    function handleDragFileSelect(evt, attrs, opts) {
      evt.stopPropagation();
      evt.preventDefault();

      fileList = evt.dataTransfer.files; // FileList object
      var fieldName = attrs.name;
      if(!files.fields[fieldName]){files.fields[fieldName] = {total: 0};} // set file length default

      checkFiles(opts, fileList, fieldName);
    }

    function checkFiles(opts, fileList, fieldName) {
      if((opts.multiple) || (files.fields[fieldName].total === 0)){ // multiple files allowed?
        if(!opts.multiple && fileList.length > 1){ // drag'n'drop check
          return $toaster.pop('error','toaster.upload', 'toaster.tomanyfiles');
        }
        if(opts.types){
          if(checkFileTypes(fileList, opts.types)){ // check file types
            startUpload(fileList, fieldName, opts.size); // start upload
          }else{
            $toaster.pop('error','toaster.upload', 'toaster.wrongfiles');
          }
        }else{
          startUpload(fileList, fieldName, opts.size);
        }
      }else{
        $toaster.pop('error','toaster.upload', 'toaster.tomanyfiles');
      }
    }

    function checkFileTypes(opts, filetypes){
      var fail = false;
      for(var i = 0; i < opts.length; i++){
        if(filetypes.indexOf(opts[i].type) < 0){
          fail = true;
        }
      }
      return !fail;
    }

    function uploadFile(file, fieldName, imagesize) {
      $socket.ready.then(function(socket) {
        files.count++;
        var stream = ss.createStream();
        var docID;

        ss(socket).emit('uploadFile', stream, {size: file.size, filename: file.name, type: file.type, field: fieldName, imagesize: imagesize});
        var readStream = ss.createBlobReadStream(file, {highWaterMark: 512 * 1024}),
          size = 0;

        streams[stream.id] = readStream;

        readStream.on('data', function(chunk) {
          size += chunk.length;
          var percent = Math.floor(size / file.size * 100);

          files.data[stream.id] = {filename: file.name, size: $bytes.toSize(file.size), percent: percent, streamID: stream.id};
        });

        readStream.on('end', function() {
          if(streams[stream.id]){
            files.count--;
            delete streams[stream.id];
            delete files.data[stream.id];
            $log.info('upload-stream completed', {streamID: stream.id});
          }
        });

        readStream.pipe(stream);

        $socket.ready.then(function(socket) {
          socket.on('streamStarted', function(data) {
            docID = data.docID;
            var index = destroyed.indexOf(docID);
            if(index === -1){toDestroy.push(docID);}
            socket.removeListener('streamStarted');
          });
        });

      });
    }

    function startUpload(fileList, fieldName, size){
      for(var i = 0; i < fileList.length; i++){
        uploadFile(fileList[i], fieldName, size);
      }
    }

    function closeStream(streamID) {
      if(streams[streamID]){
        files.count--;
        delete files.data[streamID];
        delete streams[streamID]; // remove old stream
        $log.info('upload-stream stopped by user', {streamID: streamID});

        $socket.ready.then(function(socket) {
          socket.emit('deleteFile:'+streamID);
        });
      }
    }

    $rootScope.$on('formSaved', function (event, data) {
      if(data.files){

        // remove saved files from toDestroy array
        for (var i = 0; i < data.files.length; i++) {
          var fileField = data.files[i];
          var docID = data[data.model][fileField];

          var index = toDestroy.indexOf(docID);
          if(index > -1){toDestroy.splice(index, 1);}
        }

        // remove deleted files
        if(destroyed){
          $http.post('/files/destroy', {files: destroyed}).then(function () {
            destroyed = [];
          });
        }

        toDestroy = [];
      }
    });

    $rootScope.$on('formCanceled', function () {
      if(toDestroy){
        $http.post('/files/destroy', {files: toDestroy}).then(function () {
          toDestroy = [];
        });
      }

      destroyed = [];
    });

    return {
      destroyed: function () {return destroyed;},
      toDestroy: function () {return toDestroy;},
      fileList: function () {return fileList;},
      files: files,
      addListener: addListener,
      streams: streams,
      closeStream: closeStream
    };
  })

  .directive('apupload', function($upload, $socket, $window) {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      scope:{
        model: '=',
        types: '=',
        multiple: '='
      },
      templateUrl: 'bower_components/apupload/templates/upload.html',
      link: function ($scope, element, attrs) {
        $scope.fieldName = attrs.name;

        element.find("div.input-group").on("click", function(){
          element.find("input.inputfield").trigger("click");
        });

        $scope.$watch('model', function(newValue) {
          if(newValue != undefined) {
            if ($scope.model[$scope.fieldName]) {
              $scope.filesCount = Object.keys($scope.model[$scope.fieldName]).length;
              if (!$upload.files.fields[$scope.fieldName]) {
                $upload.files.fields[$scope.fieldName] = {total: 0};
              }
              $upload.files.fields[$scope.fieldName].total = $scope.filesCount;
            }else{
              $scope.filesCount = 0;
            }
          }else{
            $scope.filesCount = 0;
          }
        });

        var field = {
          multiple: $scope.multiple,
          types: $scope.types
        };

        $scope.token = $window.sessionStorage.token;

        $scope.destroy = function(data) {
          $upload.closeStream(data.streamID);
          $scope.filesCount--;
          $upload.files.fields[$scope.fieldName].total--;

          var id = angular.copy($scope.model[$scope.fieldName][data.docID].docID);
          var index = $upload.destroyed().indexOf(id);
          if(index === -1){$upload.destroyed().push(id);}
          delete $scope.model[$scope.fieldName][data.docID];
        };

        $upload.addListener(element, attrs, field);

        $socket.ready.then(function(socket) {
          socket.on('uploadStarted', function(data) {
            if($scope.fieldName === data.field){
              if(!$scope.model){$scope.model = {};}
              if(!$scope.model[$scope.fieldName]){$scope.model[$scope.fieldName] = {};}
              $scope.model[$scope.fieldName][data.docID] = data;
              $scope.filesCount++;
              $upload.files.fields[$scope.fieldName].total++;
              $scope.model.filesReady = true;
            }
          });
        });

        $scope.$on('$destroy', function() {
          $scope.filesCount = 0;
          if (!$upload.files.fields[$scope.fieldName]) { $upload.files.fields[$scope.fieldName] = {}; }
          $upload.files.fields[$scope.fieldName].total = 0;

          $socket.ready.then(function(socket) {
            socket.removeListener('uploadStarted');
            socket.removeListener('uploadDone');
          });
        });
      }
    };
  })

  .directive('apuploadshow', function($upload) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'bower_components/apupload/templates/uploadshow.html',
      link: function ($scope) {
        $scope.files = $upload.files;
      }
    };
  })

  .directive('apuploadnav', function($upload) {
    return {
      restrict: 'E',
      replace: true,
      templateUrl: 'bower_components/apupload/templates/uploadnav.html',
      link: function ($scope) {
        $scope.files = $upload.files;

        $scope.closeStream = function(streamID) {
          $upload.closeStream(streamID);
        };
      }
    };
  })

  .directive('approgress', function() {
    return {
      restrict: 'E',
      replace: true,
      scope: {val: '='},
      templateUrl: 'bower_components/apupload/templates/uploadprogress.html',
      link: function ($scope) {
        $scope.$watch('val', function(newValue){
          if(newValue > 100){
            $scope.val = 100;
            $scope.danger = true;
          }else if(newValue < 100){
            $scope.danger = false;
          }
        });

      }
    };
  })

  .directive('apimageupload', function($upload, $socket, $http, $stateParams, $timeout, $rootScope) {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      scope:{
        ngModel: '=',
        ngChange: '&'
      },
      templateUrl: 'bower_components/apupload/templates/imageupload.html',
      link: function ($scope, element, attrs) {
        // upload options
        $scope.size = {height: attrs.imageheight, width: attrs.imagewidth};

        if(attrs.round == 'true'){
          $scope.radius = attrs.imageheight;
        }else{
          $scope.radius = '0';
        }

        var opts = {
          multiple: false,
          types: ['image/png', 'image/jpeg'],
          size: $scope.size
        };

        $scope.triggerFileSelect = function() {
          element.children()[0].click();
        };

        // add filereader to element
        $upload.addListener(element, attrs, opts);

        $socket.ready.then(function(socket) {
          // display loader image on upload
          socket.on('uploadStarted', function(data){

            if(data.field == attrs.name) {
              $scope.image = 'assets/images/loader/loader1.gif';

              if ($scope.ngModel !== undefined) {
                var id = angular.copy($scope.ngModel);
                var index = $upload.destroyed().indexOf(id);
                if (index === -1) {
                  $upload.destroyed().push(id);
                }
              }
            };
          });

          // set the correct image when uploaded
          socket.on('uploadDone', function(data){
            if(data.field == attrs.name) {
              $scope.ngModel = data.docID;
              if ($scope.ngChange) {
                $timeout(function () {
                  $scope.ngChange();
                }, 0);
              }

              $http.get('/images/' + data.docID).then(function (res) {
                $scope.image = res.data.image;
              });
            }
          });
        });

        // if($stateParams.id !== undefined){
        var imagewatcher = $scope.$watch(function($scope){
          return $scope.ngModel;
        }, function(id) {
          if(id){
            $http.get('/images/' + id).then(function (res) {
              $scope.image = res.data.image;
              imagewatcher();
            });
          }
        });
        // }

        $scope.destroyImage = function(){
          var id = angular.copy($scope.ngModel);
          var index = $upload.destroyed().indexOf(id);
          if(index === -1){$upload.destroyed().push(id);}

          $timeout(function(){
            $scope.ngModel = undefined;
            $scope.image = undefined;

            if($scope.ngChange){
              $timeout(function(){
                $scope.ngChange();
              }, 0);
            }
          }, 0);
        };

        $scope.$on('$destroy', function() {
          $socket.ready.then(function(socket) {
            socket.removeListener('uploadStarted');
            socket.removeListener('uploadDone');
          });
        });
      }
    };
  })

  .directive('multiimage', function($upload, $socket, $window, $http, $timeout) {
    return {
      restrict: 'E',
      replace: true,
      scope:{
        ngModel: '=',
      },
      templateUrl: 'bower_components/apupload/templates/multiimage.html',
      link: function ($scope, element, attrs) {
        $scope.fieldName = attrs.name;
        $scope.loader = 'assets/images/loader/loader1.gif';
        $scope.token = $window.sessionStorage.token;
        $scope.uploadCount = 0;
        $scope.images = [];

        var opts = {
          multiple: true,
          types: ['image/png', 'image/jpeg']
        };

        $upload.addListener(element, attrs, opts);

        element.find("div.placeholder").on("click", function(){
          element.find("input.fileinput").trigger("click");
        });

        $scope.removeImage = function (index) {
          var selectedImage = $scope.images[index];
          $http.delete('/files/' + selectedImage.id).then(function () {
            $scope.images.splice(index, 1);
            $scope.ngModel.splice(index, 1);
          });
        };

        $socket.ready.then(function (socket) {
          socket.on('uploadStarted', function (data) {
            if ($scope.fieldName === data.field) {
              $scope.uploadCount++;
              $upload.files.fields[$scope.fieldName].total++;
            }
          });

          socket.on('uploadDone', function (data) {
            if (data.field == attrs.name) {
              if (!$scope.ngModel) { $scope.ngModel = []; }
              $scope.ngModel.push(data.docID);
              $http.get('/images/' + data.docID).then(function (res) {
                $scope.uploadCount--;
                $scope.images.push({id: data.docID, data: res.data.image});
              });
            }
          });
        });

        $scope.$on('$destroy', function() {
          if (!$upload.files.fields[$scope.fieldName]) { $upload.files.fields[$scope.fieldName] = {}; }
          $upload.files.fields[$scope.fieldName].total = 0;

          $socket.ready.then(function (socket) {
            socket.removeListener('uploadStarted');
            socket.removeListener('uploadDone');
          });
        });
      }
    };
  });
