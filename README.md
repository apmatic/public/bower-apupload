# Demo-Server

    socketStream(socket).on('uploadFile', function(stream, data) {

      var opts = {  //Create a new Entry in The files Variable
        _id: mongoose.Types.ObjectId(),
        mode: 'w',
        filename: data.filename,
        content_type: data.type,
        metadata: {
          complete: false
          // doc_id: null
          // doc_coll: null
        }
      };

      var writestream = db.grid().createWriteStream(opts);

      var fileInfo = {
        'docID': opts._id,
        'streamID': stream.id,
        'filename': opts.filename,
        'type': opts.content_type,
        'field': data.field
      };
      // send data about the stream to the upload service
      socket.emit('streamStarted', fileInfo);
      // send data about the upload to the client, only emits once per upload
      socket.emit('uploadStarted', fileInfo);
      // user aborts file upload
      socket.on('deleteFile:'+stream.id, function() {
        writestream.destroy();

        db.grid().remove({_id: fileInfo.docID}, function(err) {
          if(err){
            logger.error('could not remove old file', {error: err});
          }
        });
      });
      // on upload done
      writestream.on('finish', function() {
        socket.emit('uploadDone', fileInfo);

        db.gridRaw().findOneAndUpdate({_id: fileInfo.docID}, {'metadata.complete': true}, function(err) {
          if(!err){
            logger.debug('upload finished', {file: fileInfo});
          }else{
            logger.error('could not set complete to true on file', {error: err});
          }
        });

      });
      // on upload error or aborted by user
      writestream.on('error', function(err) {
        if(!err){
          logger.debug('upload aborted and file deleted', {file: fileInfo});
        }else{
          logger.error('upload failed', {error: err, file: fileInfo});
        }
      });
    });
